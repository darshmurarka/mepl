<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//HOME
Route::get('/home', 'HomeController@home')->name('home');

//Landing Pages
Route::get('/tradinghome','HomeController@tradinghome')->name('tradinghome');
Route::get('/institutionalhome','HomeController@institutionalhome')->name('institutionalhome');
Route::get('/returnTradinghome','HomeController@returnTradinghome')->name('returnTradinghome');

//SKU Routes
Route::get('/skumaster', 'HomeController@addSKUMaster')->name('skumaster');
Route::get('/addsku', 'HomeController@addsku')->name('addsku');
Route::get('/createSKU', 'HomeController@createSKU')->name('createSKU');
Route::get('/updateSku', 'HomeController@updateSku')->name('updateSku');
Route::get('/deleteSku','HomeController@deleteSku')->name('deleteSku');


//User Routes
Route::get('/adduser', 'HomeController@adduser')->name('adduser');
Route::get('/createUser', 'HomeController@createUser')->name('createUser');
Route::get('/updateUser', 'HomeController@updateUser')->name('updateUser');
Route::get('/deleteUser','HomeController@deleteUser')->name('deleteUser');


//Rates & Qty Routes
Route::get('/addqty', 'HomeController@addqty')->name('addqty');
Route::get('/rates', 'HomeController@updateRates')->name('rates');
Route::get('/createQuantity', 'HomeController@createQuantity')->name('createQuantity');
Route::get('/editQuantity', 'HomeController@editQuantity')->name('editQuantity');
Route::get('/editRate', 'HomeController@editRate')->name('editRate');

//Order Routes
Route::get('/createOrder','HomeController@createOrder')->name('createOrder');

//PDF & Excel Generator
Route::get('/generatePDF','HomeController@generatePDF')->name('generatePDF');
Route::get('/generateExcelTrading','HomeController@generateExcelTrading')->name('generateExcelTrading');
Route::get('/generateExcelInstitutional','HomeController@generateExcelInstitutional')->name('generateExcelInstitutional');






