@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if(!empty($cuser))
            @if($cuser->user_type == 1)
            <div class="card">

                <div class="card-header">Add Quantity</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form action="createQuantity">
                        <div class="form-group">
                            <label for="qty">SKU:</label>
                            <select class="form-control" id="sku_id" name="sku_id">
                                @foreach($skus as $sku)
                                <option value="{{ $sku->id }}">{{ $sku->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="qty">Qty:</label>
                            <input name="quantity" type="number" class="form-control" id="qty">
                        </div>
                        <div class="form-group">
                            <label for="rate">Rate:</label>
                            <input name="rate" type="number" class="form-control" id="rate" step="0.001">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <form action="returnTradinghome">
                        <div style="text-align-last: center;">
                            <button type="submit" class="btn btn-primary">Back</button>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <br>
            <div class="card">

                <div class="card-header">Edit Quantity</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form action="editQuantity">
                        <div class="form-group">
                            <label for="qty">SKU:</label>
                            <select class="form-control" id="sku_id" name="sku_id">
                                @foreach($skus as $sku)
                                <option value="{{ $sku->id }}">{{ $sku->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="qty">Qty:</label>
                            <input name="quantity" type="number" class="form-control" id="qty">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <form action="returnTradinghome">
                        <div style="text-align-last: center;">
                            <button type="submit" class="btn btn-primary">Back</button>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <br>
            <div class="card">

                <div class="card-header">Edit Rate</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form action="editRate">
                        <div class="form-group">
                            <label for="qty">SKU:</label>
                            <select class="form-control" id="sku_id" name="sku_id">
                                @foreach($skus as $sku)
                                <option value="{{ $sku->id }}">{{ $sku->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="rate">Rate:</label>
                            <input name="rate" type="number" class="form-control" id="rate" step="0.001">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <form action="returnTradinghome">
                        <div style="text-align-last: center;">
                            <button type="submit" class="btn btn-primary">Back</button>
                        </div>
                    </form>
                </div>
            </div>
            @endif
            @endif

            @if(!empty($cuser))
            @if($cuser->user_type == 2)
            <div class="card">

                <div class="card-header">Edit Quantity</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form action="editQuantity">
                        <div class="form-group">
                            <label for="qty">SKU:</label>
                            <select class="form-control" id="sku_id" name="sku_id">
                                @foreach($skus as $sku)
                                <option value="{{ $sku->id }}">{{ $sku->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="qty">Qty:</label>
                            <input name="quantity" type="number" class="form-control" id="qty">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <form action="returnTradinghome">
                        <div style="text-align-last: center;">
                            <button type="submit" class="btn btn-primary">Back</button>
                        </div>
                    </form>
                </div>
            </div>
            @endif
            @endif


            @if(!empty($cuser))
            @if($cuser->user_type == 3)
            <div class="card">

                <div class="card-header">Edit Rate</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form action="editRate">
                        <div class="form-group">
                            <label for="qty">SKU:</label>
                            <select class="form-control" id="sku_id" name="sku_id">
                                @foreach($skus as $sku)
                                <option value="{{ $sku->id }}">{{ $sku->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="rate">Rate:</label>
                            <input name="rate" type="number" class="form-control" id="rate" step="0.001">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <form action="returnTradinghome">
                        <div style="text-align-last: center;">
                            <button type="submit" class="btn btn-primary">Back</button>
                        </div>
                    </form>
                </div>
            </div>
            @endif
            @endif
        </div>
    </div>
</div>
@endsection