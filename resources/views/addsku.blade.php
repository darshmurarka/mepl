@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Add SKU</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form action="createSKU">
                        <div class="form-group">
                            <label for="skuname">SKU Name:</label>
                            <input name="name" type="text" class="form-control" id="skuname">
                        </div>
                        <div class="form-group">
                            <label for="GST">GST:</label>
                            <input name="GST" type="number" class="form-control" id="GST" step="0.01">
                        </div>
                        <div class="form-group">
                            <label for="CESS">CESS:</label>
                            <input name="CESS" type="number" class="form-control" id="CESS" step="0.01">
                        </div>
                        <div class="form-group">
                            <label for="product_category">Product Category:</label>

                            <select class="form-control" id="product_category" name="product_category">

                                <option value=1>Trading</option>
                                <option value=2>Institutional</option>

                            </select>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <form action="returnTradinghome">
                        <div style="text-align-last: center;">
                            <button type="submit" class="btn btn-primary">Back</button>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <br>
            <div class="card">
                <div class="card-header">Edit SKU</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form action="updateSku">
                        <div class="form-group">
                            <label for="sku">SKU:</label>
                            <select class="form-control" id="sku_id" name="sku_id">
                                @foreach($skus as $sku)
                                <option value="{{ $sku->id }}">{{ $sku->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="sku_name">SKU Name:</label>
                            <input name="sku_name" type="text" class="form-control" id="sku_name">
                        </div>
                        <div class="form-group">
                            <label for="GST">GST:</label>
                            <input name="GST" type="number" class="form-control" id="GST" step="0.01">
                        </div>
                        <div class="form-group">
                            <label for="CESS">CESS:</label>
                            <input name="CESS" type="number" class="form-control" id="CESS" step="0.01">
                        </div>
                        <div class="form-group">
                            <label for="product_category">Product Category:</label>

                            <select class="form-control" id="product_category" name="product_category">

                                <option value=1>Trading</option>
                                <option value=2>Institutional</option>

                            </select>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>

                    </form>
                    <form action="returnTradinghome">
                        <div style="text-align-last: center;">
                            <button type="submit" class="btn btn-primary">Back</button>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <br>
            <div class="card">
                <div class="card-header">Delete SKU</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form action="deleteSku">
                        <div class="form-group">
                            <label for="user">SKU:</label>
                            <select class="form-control" id="sku_id" name="sku_id">
                                @foreach($skus as $sku)
                                <option value="{{ $sku->id }}">{{ $sku->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-danger">Delete</button>

                    </form>
                    <form action="returnTradinghome">
                        <div style="text-align-last: center;">
                            <button type="submit" class="btn btn-primary">Back</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection