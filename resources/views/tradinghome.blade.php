@extends('layouts.app')
@isset($message)
<div class="alert alert-success">
    <strong>{{$message}}</strong>
</div>
@endif

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Mapleleaf Trading</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>

                    @endif
                    <form method="get" action="">
                        <div class="active-cyan-3 active-cyan-4 mb-4">
                            <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="searchTag" value="{{ Request::get('searchTag') ? : '' }}">
                        </div>
                        @if(Request::get('searchTag'))
                        <a class="btn btn-primary" href="{{ route('tradinghome') }}">Clear</a>
                        @endif
                    </form>
                    <br>
                    @if(!empty($cuser))
                    @if($cuser->user_type == 1)
                    <div class="btn-group" role="group" aria-label="Tabs">
                        <button type="button" class="btn"><a class="nav-link active" href="{{ route('adduser') }}">User Management</a></button>
                        <button type="button" class="btn"> <a class="nav-link" href="{{ route('skumaster') }}">SKU Master</a></button>
                        <button type="button" class="btn"><a class="nav-link" href="{{ route('addqty') }}">Inventory Management</a></button>
                        <!-- <button type="button" class="btn"><a class="nav-link" href="{{ route('createOrder') }}">Create Order</a></button>
                        <button type="button" class="btn"><a class="nav-link" href="{{ route('addqty') }}">Order Summary</a></button> -->
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col-6">SKU Name</th>
                                <th scope="col-1">GST</th>
                                <th scope="col-1">CESS</th>
                                <th scope="col-2">SKU QTY</th>
                                <th scope="col-2">SKU Rate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($inv as $in)
                                @if($in["skus"]["product_category"]==1)
                                <tr>
                                <td>{{ $in->skus->name }}</td>
                                <td>{{ $in->skus->GST }}% </td>
                                <td>{{ $in->skus->CESS }}% </td>
                                <td>{{ $in->qty }}</td>
                                <td>{{ $in->buy_rate * $cuser->multiplier}}</td>
                            </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    {{ $inv->links() }}
                    <form action="generatePDF">
                        <div style="text-align-last: right;">
                            <input id="invisible" name="invisible" type="hidden" value="tradinghome">
                            <button type="submit" class="btn btn-danger">Download PDF</button>
                        </div>
                    </form>
                    <br>
                    <form action="generateExcelTrading">
                        <div style="text-align-last: right;">
                            <button type="submit" class="btn btn-success">Download Excel</button>
                        </div>
                    </form>
                    
                </div>
                @endif
                @if($cuser->user_type == 2)
                <div class="btn-group" role="group" aria-label="Tabs">
                    <button type="button" class="btn"><a class="nav-link" href="{{ route('addqty') }}">Inventory Management</a></button>
                    <!-- <button type="button" class="btn"><a class="nav-link" href="{{ route('createOrder') }}">Create Order</a></button>
                    <button type="button" class="btn"><a class="nav-link" href="{{ route('addqty') }}">Order Summary</a></button> -->
                </div>
                <table class="table">
                    <thead>
                    <tr>
                                <th scope="col-6">SKU Name</th>
                                <th scope="col-1">GST</th>
                                <th scope="col-1">CESS</th>
                                <th scope="col-2">SKU QTY</th>
                                <th scope="col-2">SKU Rate</th>
                            </tr>
                    </thead>
                    <tbody>
                        @foreach($inv as $in)
                        @if($in["skus"]["product_category"]==1)
                        <tr>
                                <td>{{ $in->skus->name }}</td>
                                <td>{{ $in->skus->GST }}% </td>
                                <td>{{ $in->skus->CESS }}% </td>
                                <td>{{ $in->qty }}</td>
                                <td>{{ $in->buy_rate * $cuser->multiplier}}</td>
                            </tr>
                                @endif
                        @endforeach
                    </tbody>
                </table>
                {{ $inv->links() }}
                <form action="generatePDF">
                        <div style="text-align-last: right;">
                        <input id="invisible" name="invisible" type="hidden" value="tradinghome">
                            <button type="submit" class="btn btn-danger">Download PDF</button>
                        </div>
                    </form>
                    <br>
                    <form action="generateExcelTrading">
                        <div style="text-align-last: right;">
                            <button type="submit" class="btn btn-success">Download Excel</button>
                        </div>
                    </form>
                @endif

                @if($cuser->user_type == 3)
                <div class="btn-group" role="group" aria-label="Tabs">
                    <button type="button" class="btn"><a class="nav-link" href="{{ route('addqty') }}">Inventory Management</a></button>
                    <!-- <button type="button" class="btn"><a class="nav-link" href="{{ route('createOrder') }}">Create Order</a></button>
                    <button type="button" class="btn"><a class="nav-link" href="{{ route('addqty') }}">Order Summary</a></button> -->
                </div>
                <table class="table">
                    <thead>
                    <tr>
                                <th scope="col-6">SKU Name</th>
                                <th scope="col-1">GST</th>
                                <th scope="col-1">CESS</th>
                                <th scope="col-2">SKU QTY</th>
                                <th scope="col-2">SKU Rate</th>
                            </tr>
                    </thead>
                    <tbody>
                        @foreach($inv as $in)
                        @if($in["skus"]["product_category"]==1)
                        <tr>
                                <td>{{ $in->skus->name }}</td>
                                <td>{{ $in->skus->GST }}% </td>
                                <td>{{ $in->skus->CESS }}% </td>
                                <td>{{ $in->qty }}</td>
                                <td>{{ $in->buy_rate * $cuser->multiplier}}</td>
                            </tr>
                                @endif
                        @endforeach

                    </tbody>
                </table>
                {{ $inv->links() }}
                <form action="generatePDF">
                        <div style="text-align-last: right;">
                        <input id="invisible" name="invisible" type="hidden" value="tradinghome">
                            <button type="submit" class="btn btn-danger">Download PDF</button>
                        </div>
                    </form>
                    <br>
                    <form action="generateExcelTrading">
                        <div style="text-align-last: right;">
                            <button type="submit" class="btn btn-success">Download Excel</button>
                        </div>
                    </form>
                @endif


                @if($cuser->user_type == 4)
                <!-- <div class="btn-group" role="group" aria-label="Tabs">
                    <button type="button" class="btn"><a class="nav-link" href="{{ route('createOrder') }}">Create Order</a></button>
                    <button type="button" class="btn"><a class="nav-link" href="{{ route('addqty') }}">Order Summary</a></button>
                </div> -->
                <table class="table">
                    <thead>
                    <tr>
                                <th scope="col-6">SKU Name</th>
                                <th scope="col-1">GST</th>
                                <th scope="col-1">CESS</th>
                                <th scope="col-2">SKU QTY</th>
                                <th scope="col-2">SKU Rate</th>
                            </tr>
                    </thead>
                    <tbody>
                        @foreach($inv as $in)
                        @if($in["skus"]["product_category"]==1)
                        <tr>
                                <td>{{ $in->skus->name }}</td>
                                <td>{{ $in->skus->GST }}% </td>
                                <td>{{ $in->skus->CESS }}% </td>
                                <td>{{ $in->qty }}</td>
                                <td>{{ $in->buy_rate * $cuser->multiplier}}</td>
                            </tr>
                                @endif
                        @endforeach

                    </tbody>
                </table>
                {{ $inv->links() }}
                <form action="generatePDF">
                        <div style="text-align-last: right;">
                        <input id="invisible" name="invisible" type="hidden" value="tradinghome">
                            <button type="submit" class="btn btn-danger">Download PDF</button>
                        </div>
                    </form>
                    <br>
                    <form action="generateExcelTrading">
                        <div style="text-align-last: right;">
                            <button type="submit" class="btn btn-success">Download Excel</button>
                        </div>
                    </form>
                @endif

                @if($cuser->user_type == 5)
                <!-- <div class="btn-group" role="group" aria-label="Tabs">
                    <button type="button" class="btn"><a class="nav-link" href="{{ route('createOrder') }}">Create Order</a></button>
                    <button type="button" class="btn"><a class="nav-link" href="{{ route('addqty') }}">Order Summary</a></button>
                </div> -->
                <table class="table">
                    <thead>
                    <tr>
                                <th scope="col-6">SKU Name</th>
                                <th scope="col-1">GST</th>
                                <th scope="col-1">CESS</th>
                                <th scope="col-2">SKU QTY</th>
                                <th scope="col-2">SKU Rate</th>
                            </tr>
                    </thead>
                    <tbody>
                        @foreach($inv as $in)
                        @if($in["skus"]["product_category"]==1)
                        <tr>
                                <td>{{ $in->skus->name }}</td>
                                <td>{{ $in->skus->GST }}% </td>
                                <td>{{ $in->skus->CESS }}% </td>
                                <td>{{ $in->qty }}</td>
                                <td>{{ $in->buy_rate * $cuser->multiplier}}</td>
                            </tr>
                                @endif
                        @endforeach
                    </tbody>
                </table>
                {{ $inv->links() }}
                <form action="generatePDF">
                        <div style="text-align-last: right;">
                        <input id="invisible" name="invisible" type="hidden" value="tradinghome">
                            <button type="submit" class="btn btn-danger">Download PDF</button>
                        </div>
                    </form>
                    <br>
                    <form action="generateExcelTrading">
                        <div style="text-align-last: right;">
                            <button type="submit" class="btn btn-success">Download Excel</button>
                        </div>
                    </form>
                @endif

                @if($cuser->user_type == 6)
                <!-- <div class="btn-group" role="group" aria-label="Tabs">
                    <button type="button" class="btn"><a class="nav-link" href="{{ route('createOrder') }}">Create Order</a></button>
                    <button type="button" class="btn"><a class="nav-link" href="{{ route('addqty') }}">Order Summary</a></button>
                </div> -->
                <table class="table">
                    <thead>
                    <tr>
                                <th scope="col-6">SKU Name</th>
                                <th scope="col-1">GST</th>
                                <th scope="col-1">CESS</th>
                                <th scope="col-2">SKU QTY</th>
                                <th scope="col-2">SKU Rate</th>
                            </tr>
                    </thead>
                    <tbody>
                        @foreach($inv as $in)
                        @if($in["skus"]["product_category"]==1)
                        <tr>
                                <td>{{ $in->skus->name }}</td>
                                <td>{{ $in->skus->GST }}% </td>
                                <td>{{ $in->skus->CESS }}% </td>
                                <td>{{ $in->qty }}</td>
                                <td>{{ $in->buy_rate * $cuser->multiplier}}</td>
                            </tr>
                                @endif
                        @endforeach
                    </tbody>
                 
                {{ $inv->links() }}
                <form action="generatePDF">
                        <div style="text-align-last: right;">
                        <input id="invisible" name="invisible" type="hidden" value="tradinghome">
                            <button type="submit" class="btn btn-danger">Download PDF</button>
                        </div>
                    </form>
                    <br>
                    <form action="generateExcelTrading">
                        <div style="text-align-last: right;">
                            <button type="submit" class="btn btn-success">Download Excel</button>
                        </div>
                    </form>
                @endif
                @endif
            </div>
        </div>
    </div>
</div>
</div>
@endsection