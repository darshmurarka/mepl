@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <form id="frm-example" action="{{ url('/createOrder') }}" method="GET">
                <table id="skusTable">
                    <thead>
                        <tr>
                            <th>Select</th>
                            <th>SKU Name</th>
                            <th>HSN Code</th>
                        </tr>
                    </thead>
                </table>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-md-4">
        <b>Data submitted to the server:</b><br>
        <pre id="example-console"></pre>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        // $(document).ready( function () {
        //     $('#skusTable').DataTable({
        //         processing: true,
        //         serverSide: true,
        //         ajax: base_url +  '/api/datatables/get/skusfororders'
        //     });
        // });
        $(document).ready(function() {
            $.noConflict();
            $('#skusTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: base_url +  '/api/datatables/get/skusfororders',
                columns: [
                    {data: 'checkbox', name: 'checkbox', searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'hsn_code', name: 'hsn_code'},
                ],
                pageLength: 50
            });
                
            $('#frm-example').on('submit', function(e){
                var form = this;

                // FOR TESTING ONLY
                // Prevent actual form submission
                e.preventDefault();
                
                // Output form data to a console
                $('#example-console').text($(form).serialize()); 
                console.log("Form submission", $(form).serialize()); 
                
            });
        });
    </script>
@endpush
@endsection