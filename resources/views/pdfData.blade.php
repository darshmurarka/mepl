<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Mapleleaf Internal CRM</title>

</head>

<body>
   
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col-6">SKU Name</th>
                                        <th scope="col-1">GST</th>
                                        <th scope="col-1">CESS</th>
                                        <th scope="col-2">SKU QTY</th>
                                        <th scope="col-2">SKU Rate</th>
                                    </tr>
                                </thead>
                                @if($c_route == 'institutionalhome')
                                <tbody>
                                    @foreach($inv as $in)
                                    @if($in["skus"]["product_category"]==2)
                            <tr>
                                <td>{{ $in->skus->name }}</td>
                                <td>{{ $in->skus->GST }}% </td>
                                <td>{{ $in->skus->CESS }}% </td>
                                <td>{{ $in->qty }}</td>
                                <td>{{ $in->buy_rate * $cuser->multiplier}}</td>
                            </tr>
                                @endif
                                    @endforeach
                                </tbody>
                                @endif
                                @if($c_route == 'tradinghome')
                                <tbody>
                                    @foreach($inv as $in)
                                    @if($in["skus"]["product_category"]==1)
                            <tr>
                                <td>{{ $in->skus->name }}</td>
                                <td>{{ $in->skus->GST }}% </td>
                                <td>{{ $in->skus->CESS }}% </td>
                                <td>{{ $in->qty }}</td>
                                <td>{{ $in->buy_rate * $cuser->multiplier}}</td>
                            </tr>
                                @endif
                                    @endforeach
                                </tbody>
                                @endif
                            </table>
</body>
</html>
