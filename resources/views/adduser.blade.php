@extends('layouts.app')

@section('content')
<div class="container" id="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Add User</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form action="createUser">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input name="name" type="text" class="form-control" id="name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email address:</label>
                            <input name="email" type="email" class="form-control" id="email">
                        </div>
                        <div class="form-group">
                            <label for="user_type">User Type:</label>

                            <select class="form-control" id="user_type" name="user_type">

                                <option value=1>Super User</option>
                                <option value=2>Admin(Qty Editor)</option>
                                <option value=3>Admin(Rate Editor)</option>
                                <option value=4>BigBasket</option>
                                <option value=5>Flipkart/Amazon</option>
                                <option value=6>Regional Managers</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input name="password" type="password" class="form-control" id="pwd">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>

                    </form>
                    <form action="returnTradinghome">
                        <div style="text-align-last: center;">
                            <button type="submit" class="btn btn-primary">Back</button>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <br>
            <div class="card">
                <div class="card-header">Edit User</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form action="updateUser">
                        <div class="form-group">
                            <label for="user">User:</label>
                            <select class="form-control" id="user_id" name="user_id">
                                @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="user_type">User Type:</label>

                            <select class="form-control" id="user_type" name="user_type">

                                <option value=1>Super User</option>
                                <option value=2>Admin(Qty Editor)</option>
                                <option value=3>Admin(Rate Editor)</option>
                                <option value=4>BigBasket</option>
                                <option value=5>Flipkart/Amazon</option>
                                <option value=6>Regional Managers</option>

                            </select>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>

                    </form>
                    <form action="returnTradinghome">
                        <div style="text-align-last: center;">
                            <button type="submit" class="btn btn-primary">Back</button>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <br>
            <div class="card">
                <div class="card-header">Delete User</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form action="deleteUser">
                        <div class="form-group">
                            <label for="user">User:</label>
                            <select class="form-control" id="user_id" name="user_id">
                                @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-danger">Delete</button>

                    </form>
                    <form action="returnTradinghome">
                        <div style="text-align-last: center;">
                            <button type="submit" class="btn btn-primary">Back</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection