<?php

namespace App\Http\Controllers;

use App\SkuMaster;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ApiController extends Controller
{
    //Order Placing APIS
    public function makeSKUTableForOrders(){
        $skuModel = \App\SkuMaster::query();

        return DataTables::of($skuModel)->addColumn('checkbox', function(SkuMaster $skuMaster) {
            return '<input type="checkbox" name="id[]" value="' . $skuMaster->id . '">';
        })->rawColumns(['checkbox'])->toJson();
    }
}
