<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

use App\Inventory;
use App\SkuMaster;
use App\User;
use App\skuexcel;
use App\institutionalexcel;

use DB;
use PDF;
use Excel;
use Illuminate\Support\Facades\Route;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function home()
    {
        return view('home');
    }

    public function addSKUMaster()
    {
        $skus = SkuMaster::all();

        return view('addsku',compact('skus')); 
    }

    public function updateRates()
    {
        return view('home');
    }

    public function adduser(){
        $users = User::all();
        return view('adduser',compact('users'));
    }

    public function addsku()
    {
        return view('addsku');
    }

    public function addqty()
    {
        $skus = SkuMaster::all();
        $cuser = Auth::user();
        return view('addqty',compact('skus','cuser'));
    }
    
    public function tradinghome(Request $request){
        $cuser = Auth::user();
        

        if($request['searchTag']){
            // dd('LOL');
            // $query = Inventory::with('skus')->query();
            // $query->when(request('searchTag'), function ($q) {
            //     return $q->where('skus.name', 'like', '%'. request('searchTag') .'%');
            // });
            $inv = Inventory::whereHas('skus', function ($query) {
                return $query->where('name', 'like', '%' . request('searchTag') . '%');
            })->paginate(10);

        }
        else {
            $inv = Inventory::with('skus')->paginate(10);
        }

        

        
        return view('tradinghome',
        compact('cuser','inv')
        );


    }

    public function createUser(Request $request){
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        
        $user->user_type = $request->user_type;
        $user->password = bcrypt($request->password);
        if($request->user_type==1){
            $user->multiplier = 1.0;
        }
        else if ($request->user_type == 2){
            $user->multiplier = 1.15;
        }
        else if ($request->user_type == 3) {
            $user->multiplier = 10;
        }
        else if ($request->user_type == 4) {
            $user->multiplier = 1.20;
        }
        else if ($request->user_type == 5) {
            $user->multiplier = 10;
        }
        else if ($request->user_type == 6) {
            $user->multiplier = 1.15;
        }
        $user->save();

        return redirect('tradinghome');
    }

    public function createSKU(Request $request)
    {
        $sku = new SkuMaster();
        $sku->name = $request->name;
        $sku->GST = $request->GST;
        $sku->CESS = $request->CESS;
        $sku->product_category = $request->product_category;
        $sku->save();

        return redirect('tradinghome');
    }

    public function createQuantity(Request $request)
    {

        $check = Inventory::where('sku_id', $request->sku_id)->first();

        
        if($check){
            $check->qty += $request->quantity;
            $check->buy_rate = ($check->buy_rate + $request->rate)/2;
            $check->update();
            return redirect('tradinghome');
        }

        else {
            $user = new Inventory();
            $user->sku_id = $request->sku_id;
            $user->qty = $request->quantity;
            $user->buy_rate = $request->rate;
            $user->save();
            return redirect('tradinghome');
        }
    }

    public function editQuantity(Request $request){
        $skuid = $request->sku_id;
        $skuqty = $request->quantity;
        
        DB::update('update inventory set qty = ? where sku_id = ?',[$skuqty,$skuid]);
        
        return redirect('tradinghome');
    }

    public function editRate(Request $request)
    {
        $skuid = $request->sku_id;
        $skurate = $request->rate;

        DB::update('update inventory set buy_rate = ? where sku_id = ?', [$skurate, $skuid]);

        return redirect('tradinghome');
    }

    public function returnTradinghome(){
        return redirect('tradinghome');
    }

    public function updateUser(Request $request)
    {
        $user_id = $request->user_id;
        $user_type = $request->user_type;

        DB::update('update users set user_type = ? where id = ?', [$user_type, $user_id]);

        return redirect('tradinghome');
    }

    public function deleteUser(Request $request)
    {
        $user_id = $request->user_id;

        $users = User::find($user_id);
        
        $users->delete();

        return redirect('tradinghome');
    }

    public function institutionalhome(Request $request){
        $cuser = Auth::user();
        

        if($request['searchTag']){
            // dd('LOL');
            // $query = Inventory::with('skus')->query();
            // $query->when(request('searchTag'), function ($q) {
            //     return $q->where('skus.name', 'like', '%'. request('searchTag') .'%');
            // });
            $inv = Inventory::whereHas('skus', function ($query) {
                return $query->where('name', 'like', '%' . request('searchTag') . '%');
            })->paginate(10);

        }
        else {
            $inv = Inventory::with('skus')->paginate(10);
            
        }

        

        
        return view('institutionalhome',compact('cuser','inv'));


    }


    public function updateSku(Request $request)
    {
        $sku_id = $request->sku_id;
        $sku_name = $request->sku_name;
        $GST = $request->GST;
        $CESS = $request->CESS;
        $product_category = $request->product_category;

        DB::update('update sku_master set name = ? where id = ?', [$sku_name, $sku_id]);
        DB::update('update sku_master set GST = ? where id = ?', [$GST, $sku_id]);
        DB::update('update sku_master set CESS = ? where id = ?', [$CESS, $sku_id]);
        DB::update('update sku_master set product_category = ? where id = ?', [$product_category, $sku_id]);

        return redirect('tradinghome');
    }

    public function deleteSku(Request $request)
    {
        $sku_id = $request->sku_id;

        $skus = SkuMaster::find($sku_id);
        
        $skus->delete();

        return redirect('tradinghome');
    }

    public function createOrder(){
        return view('create-order');
    }

    public function generatePDF(Request $request){
        $c_route = $request->invisible;
        $cuser = Auth::user();
        $inv = Inventory::with('skus')->get();
        $pdf = PDF::loadView('pdfData', compact('cuser','inv','c_route'));
        return $pdf->download('SKU MASTER.pdf');
    }

    public function generateExcelTrading(){
        return Excel::download(new skuexcel, 'SKUMaster.xlsx');
    }
    public function generateExcelInstitutional(){
        return Excel::download(new institutionalexcel, 'SKUMaster.xlsx');
    }


}
