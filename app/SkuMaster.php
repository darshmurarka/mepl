<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkuMaster extends Model
{
    //
    protected $table = 'sku_master';
}
