<?php
namespace App;

use App\Inventory;
use App\SkuMaster;
use App\User;
use Illuminate\Support\Facades\Auth;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class institutionalexcel implements FromCollection, WithHeadings
{
    public function collection()
    {
        $cuser = Auth::user();
        $inv = Inventory::with('skus')->get();
        $skudata = array();
        foreach($inv as $index=>$in){
            if($in["skus"]["product_category"] == 2){
            $skudata[$index]["name"] = $in["skus"]["name"];
            $skudata[$index]["gst"] = $in["skus"]["GST"];
            $skudata[$index]["cess"] = $in["skus"]["CESS"];
            $skudata[$index]["skuqty"] = $in["qty"];
            $skudata[$index]["skurate"] = $in["buy_rate"]*$cuser->multiplier;}
        }
        return collect($skudata);
    }

    public function headings(): array
    {
        return [
            'SKU Name',
            'GST',
            'CESS',
            'SKU QTY',
            'SKU Rate'
        ];

    }

}