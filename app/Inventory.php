<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    //
    protected $table = 'inventory';

    public function skus(){
        return $this->hasOne('App\SkuMaster', 'id', 'sku_id');
    }
}
